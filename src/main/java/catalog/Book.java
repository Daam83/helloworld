package catalog;

/**
 * Created by student on 01.06.17.
 */
public class Book {
    public void book(String title, String autor, int yearOfPublication) {
        this.title = title;
        this.autor = autor;
        this.yearOfPublication = yearOfPublication;
    }
    private String title;

    private String autor;

    private int yearOfPublication;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    //  public Book(String title, String autor, int yearOfPublication) {
    //    this.title = title;
    //  this.autor = autor;
    // this.yearOfPublication = yearOfPublication;
    //}


    public void setYearOfPublication(int yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }

    public void printToOutput() {

        System.out.println("Title        :" + getTitle());
        System.out.println("Author       :" + getAutor());
        System.out.println("Year publish :" + getYearOfPublication());
    }


}
