/**
 * @author student
 * Created by student on 01.06.17.
 */

import catalog.*;

import java.io.*;
import java.util.Scanner;


public class BooksCatalog {
    public static void main(String[] args) {


        Scanner inputScanner = new Scanner(System.in); // inicjalizuje skaner z konsoli.

        boolean running = true;

        Biblioteca biblioteca = new Biblioteca();
        //Shelf[] arrayShelf = new Shelf[3];
        //BookShelf[] arrayBookShelf = new BookShelf[3]; // jeśli inicjalizujemy tablice obiektów to nie możemy inicjalizować samego obiektu
        //BookShelf shelfs = new BookShelf();

        /* Infinite loop. It will read user input and make actions based on it.
         Some way to stop it should be provided. */
        while (running) {
            /* Read next user command. */
            String command = inputScanner.next();
            inputScanner.skip("\n");

            switch (command) {
                case "add": {
                    /* Add new note. */


                    System.out.println("Podaj nr regału");
                    int bookShelfIndex = inputScanner.nextInt();

                    System.out.println("Podaj nr półki");
                    int shelfIndex = inputScanner.nextInt();


                    // if (shelfs.getShelf(shelfIndex) == null) {
                    //    //arrayShelf[shelfIndex]=new Shelf();
                    //   shelfs.put(shelfIndex, new Shelf());
                    if (biblioteca.getBookShelf(bookShelfIndex) == null) { // sprawdzanbie czy tablica nie jest null
                        //arrayShelf[shelfIndex]=new Shelf();
                        biblioteca.put(bookShelfIndex, new BookShelf());
                    }
                    if (biblioteca.getBookShelf(bookShelfIndex).getShelf(shelfIndex) == null) {
                        biblioteca.getBookShelf(bookShelfIndex).put(shelfIndex, new Shelf());
                    }

                    // if (arrayBookShelf[bookShelfIndex] == null) { // sprawdzanbie czy tablica nie jest null
                    //arrayShelf[shelfIndex]=new Shelf();
                    //   arrayBookShelf[bookShelfIndex] = new BookShelf();
                    //shelfs.put(shelfIndex, new Shelf());
                    //}

                    // if (arrayBookShelf[bookShelfIndex].getBookShelf(shelfIndex) == null) {
                    //   arrayBookShelf[bookShelfIndex].put(shelfIndex, new Shelf());
                    //}

                    System.out.println("Podaj nr książki");
                    int index = inputScanner.nextInt();//Read index for new note.
                    if (biblioteca.getBookShelf(bookShelfIndex).getShelf(shelfIndex).getBook(index) == null) {
                        biblioteca.getBookShelf(bookShelfIndex).getShelf(shelfIndex).put(index, new Book());

                    }


                    /* Create local variables for title and content. Those variables exists only within nearest {} */
                    inputScanner.skip("\n");
                    System.out.println("Podaj tytuł");
                    String title = inputScanner.nextLine();
                    System.out.println("Podaj autora");
                    String author = inputScanner.nextLine();
                    System.out.println("Podaj rok");
                    int publicYear = inputScanner.nextInt();
                    inputScanner.skip("\n");
                    /* Create new note object. */

                    Book newBook = new Book();
                    newBook.setTitle(title);
                    newBook.setAutor(author);
                    newBook.setYearOfPublication(publicYear);
                    //arrayShelf[shelfIndex].put(index,newBook);
                    // shelfs.getShelf(shelfIndex).put(index, newBook);
                    // arrayBookShelf[bookShelfIndex].put(bookShelfIndex,shelfs.getShelf(bookShelfIndex).put(index, newBook));
                    // arrayBookShelf[bookShelfIndex].getBookShelf(shelfIndex).put(index, newBook);
                    biblioteca.getBookShelf(bookShelfIndex).getShelf(shelfIndex).put(index, newBook);

                    break;
                }
                case "print": {
                    /* Print Book. */
                    System.out.println("Podaj nr regału");
                    int indexBookShelf = inputScanner.nextInt();
                    System.out.println("Podaj nr półki");
                    int indexShelf = inputScanner.nextInt();
                    System.out.println("Podaj nr książki");
                    int indexBook = inputScanner.nextInt();//Read indexBook for new note.
                    //  System.out.println("Title        :" + arrayShelf[indexShelf].getBook(indexBook).getTitle());
                    // System.out.println("Title        :" + shelfs.getShelf(indexBookShelf).getBook(indexBook).getTitle());
                    //  System.out.println("Title        :" + arrayBookShelf[indexBookShelf].getBookShelf(indexBookShelf).getBook(indexBook).getTitle());
                    System.out.println("Title        :" + biblioteca.getBookShelf(indexBookShelf).getShelf(indexShelf).getBook(indexBook).getAutor());
                    //System.out.println("Author       :" + arrayShelf[indexShelf].getBook(indexBook).getAutor());
                    //System.out.println("Author       :" + arrayBookShelf[indexBookShelf].getBookShelf(indexShelf).getBook(indexBook).getAutor());
                    System.out.println("Author       :" + biblioteca.getBookShelf(indexBookShelf).getShelf(indexShelf).getBook(indexBook).getTitle());
                    // System.out.println("Year publish :" + arrayShelf[indexShelf].getBook(indexBook).getYearOfPublication());
                    //System.out.println("Year publish       :" + arrayBookShelf[indexBookShelf].getBookShelf(indexBookShelf).getBook(indexBook).getYearOfPublication());
                    System.out.println("Year publish :" + biblioteca.getBookShelf(indexBookShelf).getShelf(indexShelf).getBook(indexBook).getYearOfPublication());
// wypisuje właściwości wskazanej książki w wskazanej bibliotece i regale i półce.
                    System.out.println("-------");
                    // wypisuje właściwości wskazanej książki w wskazanej bibliotece i regale i półce.  za pomocą pośredniej metody printToOut
                    // arrayShelf[indexShelf].getBook(indexBook).printToOutput();
                    System.out.println("z book");
                    biblioteca.getBookShelf(indexBookShelf).getShelf(indexShelf).getBook(indexBook).printToOutput();
                    System.out.println("-------");
                    System.out.println("z shelf");
                    //wypisuje książke za pomocą metody print z klasy shelf przekazując do niej nr książki
                    biblioteca.getBookShelf(indexBookShelf).getShelf(indexShelf).shelfPrintOut(indexBook);
                    // biblioteca.getBookShelf(indexBookShelf).getShelf(indexShelf).shelfPrintOut(indexBook);
                    System.out.println("-------");
                    System.out.println("z bookshelf");
                    //wypisuje książke za pomocą metody print z klasy bookshelf przekazując do niej nr regału i nr książki
                    biblioteca.getBookShelf(indexBookShelf).printShelfToOutput(indexShelf, indexBook);
                    System.out.println("-------");
                    System.out.println("z biblioteki");
                    biblioteca.printBookShelf(indexBookShelf, indexShelf, indexBook);


                    break;
                }

                // TODO: print all for books, shelf and shelfs
                case "print_all": {
                    biblioteca.printAllBookShelf();
//

                    break;
                }
                case "quit": {
                    saveToFile(biblioteca, "bib");


                    running = false;
                    break;//Remember that break escapes only from switch in this case.
                }
                default: {
                    System.out.println("Valid commands are add, print and quit.");
                }
            }
        }


    }

    public static void saveToFile(Biblioteca biblioteca, String fileName) {
        try (FileWriter fw = new FileWriter("/tmp/" + fileName);
             BufferedWriter bw = new BufferedWriter(fw);) {
            for (int i = 0; i < biblioteca.bookShelfIndex(); i++) {
                bw.write("Regał NR:" + i);
                bw.newLine();
                if (biblioteca.getBookShelf(i) != null) {
                    for (int j = 0; j < biblioteca.getBookShelf(i).getBookShelfIndex(); j++) {
                        bw.write("półka NR:" + j);
                        bw.newLine();
                        if (biblioteca.getBookShelf(i).getShelf(j) != null) {
                            for (int v = 0; v < biblioteca.getBookShelf(i).getShelf(j).getIndexBook(); v++) {
                                bw.write("Ksiażka NR:" + v);
                                bw.newLine();

                                if (biblioteca.getBookShelf(i).getShelf(j).getBook(v) != null) {
                                    bw.write("Title        : " + biblioteca.getBookShelf(i).getShelf(j).getBook(v).getTitle());
                                    bw.newLine();
                                    bw.write("Author       : " + biblioteca.getBookShelf(i).getShelf(j).getBook(v).getAutor());
                                    bw.newLine();
                                    bw.write("Year publish : " + biblioteca.getBookShelf(i).getShelf(j).getBook(v).getYearOfPublication());
                                    bw.newLine();
                                } else {
                                    bw.write("Ksiażka NR:" + v);
                                    bw.newLine();
                                    bw.write("--empty--");
                                }
                            }
                        } else {
                            bw.write("--empty--");
                        }

                    }
//            bw.write(biblioteca.);


                } else {
                    bw.write("--empty--");
                }

            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }


    }
}

