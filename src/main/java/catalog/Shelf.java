package catalog;


/**
 * Created by student on 01.06.17.
 */
public class Shelf {

    private Book[] books = new Book[3];


    public void put(int index, Book book) {
        if (index >= 0 && index < books.length) {
            books[index] = book;
        }
    }

    public Book getBook(int index) {
        return books[index];
    }

    public int getIndexBook() {
        return books.length;
    }

    public void shelfPrintOut(int index) {
        if (getBook(index) != null) {
            if (index >= 0 && index < books.length) {
                System.out.println("Książka NR:" + index);
                getBook(index).printToOutput();
            }

        }
    }

    public void printOutAllBook() {
        for (int i = 0; i < books.length; i++) {
            if (getBook(i) != null) {
                System.out.println("-----------");
                System.out.println("Książka NR:" + i);
                printOutAllBook(books[i]);
            } else {
                System.out.println("-----------");
                System.out.println("Książka NR:" + i);
                System.out.println("--empty--");
            }
        }
    }

    public static void printOutAllBook(Book book) {

        if (book != null) {
            book.printToOutput();

        } else {
            System.out.println("-------");
            System.out.println("EMPTY");
            }

    }


}
