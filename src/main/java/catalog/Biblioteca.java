package catalog;

/**
 * Created by student on 03.06.17.
 */
public class Biblioteca {

    private BookShelf[] bibliotec = new BookShelf[3];


    public void put(int indexBookShelf, BookShelf bookShelf) {
        if (indexBookShelf >= 0 && indexBookShelf <= bibliotec.length) {
            bibliotec[indexBookShelf] = bookShelf;
        }
    }

    public BookShelf getBookShelf(int indexBookShelf) {
        return bibliotec[indexBookShelf];
    }

    public int bookShelfIndex() {
        return bibliotec.length;
    }

    public void printBookShelf(int indexBookshelf, int indexShelf, int indexBook) {
        if (getBookShelf(indexBookshelf) != null) {
            System.out.println("Regał NR:" + indexBookshelf);
            getBookShelf(indexBookshelf).printShelfToOutput(indexShelf, indexBook);
        }
    }

    public void printAllBookShelf() {
        for (int i = 0; i < bibliotec.length; i++) {
            if (bibliotec[i] != null) {
                System.out.println("~~~~~~~~~~~~~~~~");
                System.out.println("Regał NR: " + i);
                printALLBookShelf(bibliotec[i]);
            } else {
                System.out.println("~~~~~~~~~~~~~~~~");
                System.out.println("Regał NR: " + i);
                System.out.println("--empty--");
            }
        }
    }

    public static void printALLBookShelf(BookShelf bookShelf) {
        if (bookShelf != null) {
            bookShelf.printALLShelf();
        }
    }


}
